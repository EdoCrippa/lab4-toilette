# UNISEX TOILETTE EXAMPLE #

Un ristorante ha una **toilette** unica per uomini e donne ed ha una capacità limitata a **N** persone. Si assuma che ogni utente della toilette (**Man**/**Woman**) sia rappresentato in Java da un *thread* e che il bagno (classe `Toilettte`) sia un oggetto Java condiviso.  Implementare i thread utenti della toilette e completare la definizione della classe `Toilette` realizzando uno schema di sincronizzazione con **Lock** e **Condition variable** del package `java.util.concurrent.locks` che garantisca i seguenti vincoli:

* nella **toilette** non possono esserci contemporaneamente **uomini** e **donne**
* nell'accesso alla **toilette**, le **donne** hanno la priorità sugli **uomini**

Tali condizioni di sincronizzazione devono riflettersi nel codice della classe `Toilette` nel modo seguente:

* Il thread `Woman` richiamerà il metodo `womanIn()` per entrare nel bagno e deve essere sospeso:
* * se ci sono **uomini** all'interno della **toilette**
* * se la **toilette** è piena.
* Il thread `Man` esegue il metodo `manIn()` per entrare nella **toilette** e deve essere sospeso:
* * se ci sono **donne** all'interno della **toilette**
* * se la **toilette** è piena.
* * se ci sono **donne** in attesa
package it.unibg.so.lab4;

public class Driver {

	public static void main(String[] args) {               
		int womenNumber =10;
		int menNumber =5;
		Toilette toilette = new Toilette(3);        
		Man man[] = new Man[menNumber];
		Woman woman[] = new Woman[womenNumber];

		for (int i=0; i<menNumber; i++){
			man[i]=new Man(toilette,i+1);
			man[i].start();
		}
		for (int i=0; i<womenNumber; i++){
			woman[i]=new Woman(toilette,i+1);
			woman[i].start();
		}
		for (int i=0; i<menNumber ; i++ ){
			try {
				man[i].join();
			}catch (Exception e){}
		}
		for (int i=0;i<womenNumber ;i++ ){
			try {
				woman[i].join();
			}catch (Exception e){}
		}   
	}
}

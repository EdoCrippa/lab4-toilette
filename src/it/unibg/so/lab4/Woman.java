package it.unibg.so.lab4;

public class Woman extends Thread {

	private Toilette t; //riferimento alla risorsa condivisa toilette

	public Woman(Toilette t, int id){
		this.t = t;
		this.setName(String.valueOf(id));
	}

	public void run(){      
		try {
			t.womanIn();
			//Aspetta un tempo casuale (max. 10 sec.)
			sleep((int)(Math.random()*10000));
			t.womanOut();          
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}

package it.unibg.so.lab4;

public class Man extends Thread{
	
	private Toilette t; //riferimento alla risorsa condivisa toilette

	public Man(Toilette t, int id){
		this.t = t;
		this.setName(String.valueOf(id));
	}

	public void run(){
		try {
			t.manIn();
			//Aspetta un tempo casuale (max. 10 sec.)
			sleep((int)( Math.random()*10000) );
			t.manOut(); 

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}